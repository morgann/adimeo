<?php

namespace Drupal\adimeo_events\Plugin\Block;

use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * Class RelatedEventsBlock for display related events.
 *
 * @package Drupal\adimeo_events\Plugin\Block
 *
 * @Block(
 *  id = "related_events_block",
 *  admin_label = @Translation("Related Events Block"),
 *  context_definitions = {
 *    "node" = @ContextDefinition("entity:node", label = @Translation("Node")),
 *  }
 * )
 */
class RelatedEventsBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The max number of events.
   *
   * @var int
   */
  public const EVENTS_MAX = 3;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The formatted date for query
   *
   * @var string
   */
  protected $formatted;

  /**
   * Event term type.
   *
   * @var int
   */
  protected $event_type;

  /**
   * Constructs of RelatedEventsBlock.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;

    $date = new DrupalDateTime('now');
    $date->setTimezone(new \DateTimezone(DateTimeItemInterface::STORAGE_TIMEZONE));
    $this->formatted = $date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Get context node definition (Also useful for auto caching)
    $node = $this->getContextValue('node');
    $nodes = [];

    // Check context and check again bundle (Even if set on block config)
    if($node && $node->bundle() === "event") {
      $this->event_type = $node->get("field_event_type")->first()->entity->id();

      // Get events with same term type
      $events = $this->getEvents(self::EVENTS_MAX);
      $diff = self::EVENTS_MAX - count($events);

      // If not max, get events with other term type
      if($diff) {
        $other_events = $this->getEvents($diff, false);
        $events = array_merge($events, $other_events);
      }

      // Sort again if events with other term type are set
      $events = $this->entityTypeManager->getStorage('node')->loadMultiple($events);
      usort($events, function($a, $b){
        return $a->get("field_date_range")->value <=> $b->get("field_date_range")->value;
      });

      // Rendering in teaser display mode
      $view_builder = $this->entityTypeManager->getViewBuilder("node");
      foreach($events as $event) {
        $nodes[] = $view_builder->view($event, "teaser");
      }
    }

    return [
      '#theme' => 'related_events',
      '#nodes' => $nodes,
      '#cache' => ['tags' => ['node_list:event']],
    ];
  }

  /**
   * Get events from entity query.
   *
   * @param int $range
   *   Max range in query.
   * @param bool $match
   *   Search with same term or not.
   *
   * @return array
   *   Array of entities ids.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getEvents(int $range, bool $match = true) {
    $node = $this->getContextValue('node');
    $query = $this->entityTypeManager->getStorage('node')
      ->getQuery()
      ->accessCheck(TRUE)
      ->condition('type', 'event')
      ->condition('nid', $node->id(), '<>')
      ->condition('status', 1)
      ->condition('field_date_range.end_value', $this->formatted, '>')
      ->sort('field_date_range.value')
      ->range(0, $range);

    if($match){
      $query->condition('field_event_type', $this->event_type);
    } else {
      $query->condition('field_event_type', $this->event_type, '<>');
    }

    return $query->execute();
  }
}
