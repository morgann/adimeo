<?php

namespace Drupal\adimeo_events\Services;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\Core\Queue\QueueFactory;

/**
 * Class EventsNodesService for unpublish events.
 *
 * @package Drupal\adimeo_events\Services
 */
class EventsNodesService {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The queue service.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The formatted date for query
   *
   * @var string
   */
  protected $formatted;

  /**
   * Constructs of EventsNodesService.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, QueueFactory $queue_factory) {
    $this->entityTypeManager = $entityTypeManager;
    $this->queueFactory = $queue_factory;

    $date = new DrupalDateTime('now');
    $date->setTimezone(new \DateTimezone(DateTimeItemInterface::STORAGE_TIMEZONE));
    $this->formatted = $date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
  }

  /**
   * Unpublish old events nodes.
   *
   * @return void
   *   Queue task increase.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function unpublishEventsNodes() {
    $queue = $this->queueFactory->get('unpublish_events_nodes_queue');
    $events = $this->entityTypeManager->getStorage('node')
      ->getQuery()
      ->accessCheck(TRUE)
      ->condition('type', 'event')
      ->condition('status', 1)
      ->condition('field_date_range.end_value', $this->formatted, '<')
      ->execute();

    foreach($events as $event) {
      $queue->createItem($event);
    }
  }

}
