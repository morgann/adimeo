# Test Adimeo

## 1 - Rappel des tâches

### 1.1 Faire un bloc custom (plugin annoté)
* s'affichant sur la page de détail d'un événement ;
* et affichant 3 autres événements du même type (taxonomie) que l'événement courant, ordonnés par date de début (asc), et dont la date de fin n'est pas dépassée ;
* S'il y a moins de 3 événements du même type, compléter avec un ou plusieurs événements d'autres types, ordonnés par date de début (asc), et dont la date de fin n'est pas dépassée.

### 1.2 Faire une tache cron
Qui dépublie, **de la manière la plus optimale,** les événements dont la date de fin est dépassée à l'aide d'un **QueueWorker**.

## 2 - Installation

Les configurations ont été exportées, voici la procédure :
* Création du **settings.local.php** dans le site par défaut
* Import de la base de données **dump.sql.zip**
* Extraction du dump **files.zip** et copie dans le site par défaut
* Lancer les commandes suivantes :
  * `composer install`
  * `drush deploy` OU  `drush cim && drush cr`

## 3 - Informations

* Le mot de passe admin est inchangé
* Le code a été commenté pour faciliter la lecture
